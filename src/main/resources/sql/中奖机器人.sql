CREATE TABLE `tg_draw` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `block` varchar(255) DEFAULT '' COMMENT '区块',
                           `block_height` varchar(255) DEFAULT '' COMMENT '区块高度',
                           `block_code` bigint(20) DEFAULT '0' COMMENT 'block数字',
                           `open_time` bigint(20) DEFAULT '0' COMMENT '开奖时间',
                           `draw_code` bigint(20) DEFAULT '0' COMMENT '中奖编号',
                           `user_name` varchar(255) DEFAULT '',
                           `draw_tg_uid` varchar(255) DEFAULT '' COMMENT '中奖tguid',
                           `note` varchar(255) DEFAULT '' COMMENT '备注',
                           `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                           `lastmodify` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tg_draw_activity` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `activity_code` varchar(255) DEFAULT '' COMMENT '活动编号',
                                    `activity_desc` varchar(255) DEFAULT '' COMMENT '活动描述',
                                    `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `draw_admin_user` varchar(255) DEFAULT '' COMMENT '开奖人员username',
                                    `not_join_username` varchar(255) DEFAULT '' COMMENT '不能参与人员名单',
                                    `note` varchar(255) DEFAULT '',
                                    `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                    `lastmodify` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                    `height` varchar(255) DEFAULT '' COMMENT '区块高度',
                                    `block_hash` varchar(255) DEFAULT '' COMMENT '区块hash',
                                    `lottery_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开奖时间',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#抽奖活动插入
insert  into `tg_draw_activity`(`id`,`activity_code`,`activity_desc`,`start_time`,`end_time`,`draw_admin_user`,`not_join_username`,`note`,`create_time`,`lastmodify`,`height`,`block_hash`,`lottery_time`) values (1,'20211203','20211203','2021-12-03 04:22:35','2021-12-05 04:22:35','','','','2021-12-03 04:22:35','2021-12-03 04:25:25','','','2021-12-03 04:22:35');


CREATE TABLE `tg_draw_record` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `tg_uid` varchar(255) DEFAULT '' COMMENT 'tg_uid',
                                  `user_name` varchar(255) DEFAULT '' COMMENT 'user_name',
                                  `code` bigint(20) DEFAULT '0' COMMENT 'code',
                                  `note` varchar(255) DEFAULT '' COMMENT 'note',
                                  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `lastmodify` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
