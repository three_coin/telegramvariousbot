package com.bot.telegramvariousbot.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MysqlEntityUtil {
    private static final MysqlEntityUtil INSTANCE = new MysqlEntityUtil();

    private String tableName;// 表名
    private String[] colNames; // 列名数组
    private String[] colTypes; // 列名类型数组
    //private String[] colNotes;
    private List<String> colNotes; // 列名注释
    private int[] colSizes; // 列名大小数组
    private boolean needUtil = false; // 是否需要导入包java.util.*
    private boolean needSql = false; // 是否需要导入包java.sql.*
    private boolean bigSql = false; // 是否需要导入包java.math.BigDecimal;
    private boolean Swagger = false; // 是否需要Swagger注释
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String SQL = "SELECT * FROM ";// 数据库操作

    private static final String URL = "jdbc:mysql://localhost:3306/bot?useCursorFetch=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC";
    private static final String NAME = "root";
    private static final String PASS = "zfzf123";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private String packageOutPath = "com.bot.telegramvariousbot.model";// 指定实体生成所在包的路径
    private String authorName = "zhou";// 作者名字
    private String[] generateTables = {"tg_draw","tg_draw_record","tg_draw_activity","sociality_message_log"} ;//指定需要生成的表的表名，全部生成设置为null




    /**
     * 类的构造方法
     */
    private MysqlEntityUtil() {
    }

    /**
     * @return
     * @description 生成class的所有内容
     * @author paul
     * @date 2017年8月18日 下午5:30:07
     * @update 2017年8月18日 下午5:30:07
     * @version V1.0
     */
    private String parse() {
        StringBuffer sb = new StringBuffer();
        sb.append("package " + packageOutPath + ";\r\n");
        sb.append("\r\n");
        // 判断是否导入工具包
        if(Swagger){
            sb.append("import io.swagger.annotations.ApiModelProperty;\r\n");
        }
        /*if (needUtil) {
            sb.append("import java.util.Date;\r\n");
        }*/
        if(bigSql){
            sb.append("import java.math.BigDecimal;\r\n");
        }
        if (needSql) {
            sb.append("import java.sql.*;\r\n");
        }
        sb.append("import org.springframework.jdbc.core.BeanPropertyRowMapper;\r\n" +
                "import org.springframework.jdbc.core.RowMapper;\r\n");
        // 注释部分
        sb.append("/**\r\n");
        sb.append(" * table name:  " + tableName + "\r\n");
        sb.append(" * author name: " + authorName + "\r\n");
        //sb.append(" * create time: " + SDF.format(new Date()) + "\r\n");
        sb.append(" */ \r\n");
        // 实体部分
        sb.append("public class " + getTransStr(tableName, true) + "{\r\n\r\n");
        sb.append("  public static final RowMapper<" + getTransStr(tableName, true) + "> ROWMAPPER = BeanPropertyRowMapper.newInstance(" + getTransStr(tableName, true) + ".class);\r\n\r\n");
        processAllAttrsInsertSql(sb);
        processAllAttrs(sb);// 属性
        sb.append("\r\n");
        processAllMethod(sb);// get set方法
        //processToString(sb);
        sb.append("}\r\n");
        return sb.toString();
    }

    /**
     * @param sb
     * @description 生成所有成员变量
     * @author paul
     * @date 2017年8月18日 下午5:15:04
     * @update 2017年8月18日 下午5:15:04
     * @version V1.0
     */
    private void processAllAttrs(StringBuffer sb) {
        for (int i = 0; i < colNames.length; i++) {
            String tt = sqlType2JavaType(colTypes[i]);
            if (tt != null) {
                //注释
                if(Swagger){
                    //@ApiModelProperty(value = "给用户显示的ID",example = "给用户显示的ID")
                    sb.append("\t@ApiModelProperty(value = \""+colNotes.get(i)+"\")\r\n");
                }
                sb.append("\tprivate " + tt + " " + getTransStr(colNames[i], false) + ";\r\n");
            }
        }
    }

    private void processAllAttrsInsertSql(StringBuffer sb) {
        //insert app_user_invite (uid,invite_uid) values (?,?)
        sb.append("  //insert " +tableName+" (");
        for (int i = 0; i < colNames.length; i++) {
            sb.append(colNames[i]);
            if(i!=colNames.length-1){
                sb.append(",");
            }
        }
        sb.append(") values (");
        for (int i = 0; i < colNames.length; i++) {
            sb.append(":"+getTransStr(colNames[i], false));
            if(i!=colNames.length-1){
                sb.append(",");
            }
        }
        sb.append(")\r\n\r\n");
    }

    /**
     * @param sb
     * @description 生成所有get/set方法
     * @author paul
     * @date 2017年8月18日 下午5:14:47
     * @update 2017年8月18日 下午5:14:47
     * @version V1.0
     */
    private void processAllMethod(StringBuffer sb) {
        for (int i = 0; i < colNames.length; i++) {
            String tt = sqlType2JavaType(colTypes[i]);
            if (tt != null) {
                sb.append("\tpublic void set" + getTransStr(colNames[i], true) + "(" + sqlType2JavaType(colTypes[i]) + " "
                        + getTransStr(colNames[i], false) + "){\r\n");
                sb.append("\t\tthis." + getTransStr(colNames[i], false) + "=" + getTransStr(colNames[i], false) + ";\r\n");
                sb.append("\t}\r\n");
                sb.append("\tpublic " + tt + " get" + getTransStr(colNames[i], true) + "(){\r\n");
                sb.append("\t\treturn " + getTransStr(colNames[i], false) + ";\r\n");
                sb.append("\t}\r\n");
            }
        }
    }

    /**
     * @param str 传入字符串
     * @return
     * @description 将传入字符串的首字母转成大写
     * @author paul
     * @date 2017年8月18日 下午5:12:12
     * @update 2017年8月18日 下午5:12:12
     * @version V1.0
     */
    private String initCap(String str) {
        char[] ch = str.toCharArray();
        if (ch[0] >= 'a' && ch[0] <= 'z')
            ch[0] = (char) (ch[0] - 32);
        return new String(ch);
    }

    /**
     * @return
     * @description 将mysql中表名和字段名转换成驼峰形式
     * @author paul
     * @date 2017年8月18日 下午4:55:07
     * @update 2017年8月18日 下午4:55:07
     * @version V1.0
     */
    private String getTransStr(String before, boolean firstChar2Upper) {
        //不带"_"的字符串,则直接首字母大写后返回
        if (!before.contains("_"))
            return firstChar2Upper ? initCap(before) : before;
        String[] strs = before.split("_");
        StringBuffer after = null;
        if (firstChar2Upper) {
            after = new StringBuffer(initCap(strs[0]));
        } else {
            after = new StringBuffer(strs[0]);
        }
        if (strs.length > 1) {
            for (int i = 1; i < strs.length; i++)
                after.append(initCap(strs[i]));
        }
        return after.toString();
    }

    /**
     * @return
     * @description 查找sql字段类型所对应的Java类型
     * @author paul
     * @date 2017年8月18日 下午4:55:41
     * @update 2017年8月18日 下午4:55:41
     * @version V1.0
     */
    private String sqlType2JavaType(String sqlType) {
        if (sqlType.equalsIgnoreCase("bit")) {
            return "boolean";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            return "byte";
        } else if (sqlType.equalsIgnoreCase("smallint")) {
            return "short";
        } else if (sqlType.equalsIgnoreCase("int")) {
            return "int";
        } else if (sqlType.equalsIgnoreCase("bigint")) {
            return "long";
        } else if (sqlType.equalsIgnoreCase("float")) {
            return "float";
        }else if (sqlType.equalsIgnoreCase("decimal") || sqlType.equalsIgnoreCase("numeric")
                || sqlType.equalsIgnoreCase("real") || sqlType.equalsIgnoreCase("money")
                || sqlType.equalsIgnoreCase("smallmoney")) {
            return "BigDecimal";
        } else if (sqlType.equalsIgnoreCase("varchar") || sqlType.equalsIgnoreCase("char")
                || sqlType.equalsIgnoreCase("nvarchar") || sqlType.equalsIgnoreCase("nchar")
                || sqlType.equalsIgnoreCase("text")|| sqlType.equalsIgnoreCase("longtext")) {
            return "String";
        } else if (sqlType.equalsIgnoreCase("date")) {
            return "Date";
        }else if (sqlType.equalsIgnoreCase("datetime")) {
            return "Timestamp";
        }else if (sqlType.equalsIgnoreCase("timestamp")) {
            return "Timestamp";
        }
        return null;
    }

    /**
     * @throws Exception
     * @description 生成方法
     * @author paul
     * @date 2017年8月18日 下午2:04:20
     * @update 2017年8月18日 下午2:04:20
     * @version V1.0
     */
    private void generate() throws Exception {
        //与数据库的连接
        Connection con;
        PreparedStatement pStemt = null;
        Class.forName(DRIVER);
        con = DriverManager.getConnection(URL, NAME, PASS);
        System.out.println("connect database success...");
        //获取数据库的元数据
        DatabaseMetaData db = con.getMetaData();
        //是否有指定生成表，有指定则直接用指定表，没有则全表生成
        List<String> tableNames = new ArrayList<>();
        if (generateTables == null) {
            //从元数据中获取到所有的表名
            ResultSet rs = db.getTables("da", null, null, new String[]{"TABLE"});
            while (rs.next()) tableNames.add(rs.getString(3));
        } else {
            for (String tableName : generateTables) tableNames.add(tableName);
        }
        String tableSql;
        PrintWriter pw = null;
        for (int j = 0; j < tableNames.size(); j++) {
            tableName = tableNames.get(j);
            tableSql = SQL + tableName;
            pStemt = con.prepareStatement(tableSql);
            ResultSetMetaData rsmd = pStemt.getMetaData();
            int size = rsmd.getColumnCount();
            colNames = new String[size];
            colTypes = new String[size];
            colSizes = new int[size];
            //获取所需的信息
            for (int i = 0; i < size; i++) {
                colNames[i] = rsmd.getColumnName(i + 1);
                colTypes[i] = rsmd.getColumnTypeName(i + 1);
                if (colTypes[i].equalsIgnoreCase("date"))
                    needUtil = true;
                if (colTypes[i].equalsIgnoreCase("timestamp")||colTypes[i].equalsIgnoreCase("image") || colTypes[i].equalsIgnoreCase("text"))
                    needSql = true;
                if (colTypes[i].equalsIgnoreCase("decimal") || colTypes[i].equalsIgnoreCase("numeric")|| colTypes[i].equalsIgnoreCase("real")|| colTypes[i].equalsIgnoreCase("money")|| colTypes[i].equalsIgnoreCase("smallmoney"))
                    bigSql = true;
                colSizes[i] = rsmd.getColumnDisplaySize(i + 1);
            }
            colNotes = getColumnComments(tableSql,tableName);
            //解析生成class的所有内容
            String content = parse();
            //输出生成文件
            File directory = new File("");
            String dirName = directory.getAbsolutePath() + "/src/main/java/" + packageOutPath.replace(".", "/");
            File dir = new File(dirName);
            if (!dir.exists() && dir.mkdirs()) System.out.println("generate dir 【" + dirName + "】");
            String javaPath = dirName + "/" + getTransStr(tableName, true) + ".java";
            FileWriter fw = new FileWriter(javaPath);
            pw = new PrintWriter(fw);
            pw.println(content);
            pw.flush();
            System.out.println("create class 【" + tableName + "】");
        }
        if (pw != null) {
            pw.close();
        }
        con.close();
    }

    public static List<String> getColumnComments(String tableSql,String tableName) throws Exception {
        List<String> columnTypes = new ArrayList<>();
        //与数据库的连接
        Connection con;
        PreparedStatement pStemt = null;
        Class.forName(DRIVER);
        con = DriverManager.getConnection(URL, NAME, PASS);

        List<String> columnComments = new ArrayList<>();//列名注释集合
        ResultSet rs = null;
        try {
            pStemt = con.prepareStatement(tableSql);
            rs = pStemt.executeQuery("show full columns from " + tableName);
            while (rs.next()) {
                columnComments.add(rs.getString("Comment"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();

                } catch (SQLException e) {
                   System.out.println("getColumnComments close ResultSet and connection failure");
                }
            }
        }
        return columnComments;
    }

    /**
     * @param args
     * @description 执行方法
     * @author paul
     * @date 2017年8月18日 下午2:03:35
     * @update 2017年8月18日 下午2:03:35
     * @version V1.0
     */
    public static void main(String[] args) {
        try {
            INSTANCE.generate();
            System.out.println("generate classes success!");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
