package com.bot.telegramvariousbot.dao;


import com.bot.telegramvariousbot.model.SocialityMessageLog;
import com.bot.telegramvariousbot.model.TgDraw;
import com.bot.telegramvariousbot.model.TgDrawActivity;
import com.bot.telegramvariousbot.model.TgDrawRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LuckDrawDaoExt extends BaseService {
    private static Logger logger = LoggerFactory.getLogger(LuckDrawDaoExt.class);

    public int insertTgDrawRecordBean(TgDrawRecord tgDrawRecord){
        String sql = "insert tg_draw_record (draw_day,tg_uid,user_name,code,note,activity_id) values (:drawDay,:tgUid,:userName,:code,:note,:activityId)";
        try{
            return new NamedParameterJdbcTemplate(jdbcTemplateW).update(sql, new BeanPropertySqlParameterSource(tgDrawRecord));
        }catch (Exception e){
            logger.error("insert insertTgDrawRecordBean is error",e);
            return -1;
        }
    }

    public TgDrawActivity getTgDrawById(int id) {
        try {
            String sql = "SELECT * FROM tg_draw_activity WHERE id =?";
            List<TgDrawActivity> list = jdbcTemplateR.query(sql, TgDrawActivity.ROWMAPPER, id);
            if (list != null && list.size() != 0) {
                return list.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("getTgDrawById is error", e);
            return null;
        }
    }

    public int insertTgDrawBean(TgDraw tgDraw){
        String sql = "insert tg_draw (draw_day,block,block_height,block_code,open_time,draw_code,user_name,draw_tg_uid,note,activity_id) values (:drawDay,:block,:blockHeight,:blockCode,:openTime,:drawCode,:userName,:drawTgUid,:note,:activityId)";
        try{
            return new NamedParameterJdbcTemplate(jdbcTemplateW).update(sql, new BeanPropertySqlParameterSource(tgDraw));
        }catch (Exception e){
            logger.error("insert insertTgDrawBean is error",e);
            return -1;
        }
    }

    public TgDrawRecord getTgDrawRecordByTgUid(String tg_uid,int activityId,String day) {
        try {
            String sql = "SELECT * FROM tg_draw_record WHERE tg_uid =? AND activity_id = ? AND draw_day = ?";
            List<TgDrawRecord> list = jdbcTemplateR.query(sql, TgDrawRecord.ROWMAPPER, tg_uid,activityId,day);
            if (list != null && list.size() != 0) {
                return list.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("getTgDrawRecordByTgUid is error", e);
            return null;
        }
    }

    public TgDrawRecord getTgDrawRecordByDrawCode(long code,int activityId,String day) {
        try {
            String sql = "SELECT *,ABS(`CODE`-?) AS absnum FROM `tg_draw_record` where activity_id=? AND draw_day=? ORDER BY absnum,create_time LIMIT 1;";
            List<TgDrawRecord> list = jdbcTemplateR.query(sql, TgDrawRecord.ROWMAPPER, code,activityId,day);
            if (list != null && list.size() != 0) {
                return list.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("getTgDrawRecordByDrawCode is error", e);
            return null;
        }
    }

    public TgDraw getTgDrawLimitOne(int activityId,String day) {
        try {
            String sql = "SELECT * FROM `tg_draw` where activity_id=? and draw_day = ? LIMIT 1";
            List<TgDraw> list = jdbcTemplateR.query(sql, TgDraw.ROWMAPPER,activityId,day);
            if (list != null && list.size() != 0) {
                return list.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("getTgDrawLimitOne is error", e);
            return null;
        }
    }
    public SocialityMessageLog getSMLogByTGUid(String chat_id,String user_id,String day) {
        try {
            String sql = "SELECT * FROM `sociality_message_log` where chat_id=? and user_id=? and DATE_FORMAT(create_time,'%Y-%m-%d')=?  LIMIT 1";
            List<SocialityMessageLog> list = jdbcTemplateR.query(sql, SocialityMessageLog.ROWMAPPER,chat_id,user_id,day);
            if (list != null && list.size() != 0) {
                return list.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("getSMLogByTGUid is error", e);
            return null;
        }
    }

    public Integer sumYesterdayDrawNum(int activityId,String day) {
        String sql = "SELECT count(*) FROM `tg_draw_record` where activity_id=? and draw_day=?";
        try {
            return jdbcTemplateR.queryForObject(sql,Integer.class,activityId,day);
        }catch (Exception e){
            logger.error("sumYesterdayDrawNum is error",e);
            return 0;
        }
    }


}
