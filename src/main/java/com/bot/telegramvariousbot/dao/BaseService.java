package com.bot.telegramvariousbot.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class BaseService {

    @Resource
    protected JdbcTemplate jdbcTemplateR;

    @Resource
    protected JdbcTemplate jdbcTemplateW;
    
}
