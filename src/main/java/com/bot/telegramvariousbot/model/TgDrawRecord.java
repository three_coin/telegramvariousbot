package com.bot.telegramvariousbot.model;

import java.sql.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
/**
 * table name:  tg_draw_record
 * author name: zhou
 */ 
public class TgDrawRecord{

  public static final RowMapper<TgDrawRecord> ROWMAPPER = BeanPropertyRowMapper.newInstance(TgDrawRecord.class);

  //insert tg_draw_record (id,draw_day,tg_uid,user_name,code,note,create_time,lastmodify,activity_id) values (:id,:drawDay,:tgUid,:userName,:code,:note,:createTime,:lastmodify,:activityId)

	private int id;
	private String drawDay;
	private String tgUid;
	private String userName;
	private long code;
	private String note;
	private Timestamp createTime;
	private Timestamp lastmodify;
	private int activityId;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setDrawDay(String drawDay){
		this.drawDay=drawDay;
	}
	public String getDrawDay(){
		return drawDay;
	}
	public void setTgUid(String tgUid){
		this.tgUid=tgUid;
	}
	public String getTgUid(){
		return tgUid;
	}
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return userName;
	}
	public void setCode(long code){
		this.code=code;
	}
	public long getCode(){
		return code;
	}
	public void setNote(String note){
		this.note=note;
	}
	public String getNote(){
		return note;
	}
	public void setCreateTime(Timestamp createTime){
		this.createTime=createTime;
	}
	public Timestamp getCreateTime(){
		return createTime;
	}
	public void setLastmodify(Timestamp lastmodify){
		this.lastmodify=lastmodify;
	}
	public Timestamp getLastmodify(){
		return lastmodify;
	}
	public void setActivityId(int activityId){
		this.activityId=activityId;
	}
	public int getActivityId(){
		return activityId;
	}
}

