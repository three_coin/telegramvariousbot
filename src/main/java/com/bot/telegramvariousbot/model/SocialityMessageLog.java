package com.bot.telegramvariousbot.model;

import java.sql.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
/**
 * table name:  sociality_message_log
 * author name: zhou
 */ 
public class SocialityMessageLog{

  public static final RowMapper<SocialityMessageLog> ROWMAPPER = BeanPropertyRowMapper.newInstance(SocialityMessageLog.class);

  //insert sociality_message_log (id,chat_id,chat_name,user_id,username,msg_source,msg_type,msg_id,create_time,lastmodify) values (:id,:chatId,:chatName,:userId,:username,:msgSource,:msgType,:msgId,:createTime,:lastmodify)

	private int id;
	private String chatId;
	private String chatName;
	private String userId;
	private String username;
	private byte msgSource;
	private int msgType;
	private String msgId;
	private Timestamp createTime;
	private Timestamp lastmodify;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setChatId(String chatId){
		this.chatId=chatId;
	}
	public String getChatId(){
		return chatId;
	}
	public void setChatName(String chatName){
		this.chatName=chatName;
	}
	public String getChatName(){
		return chatName;
	}
	public void setUserId(String userId){
		this.userId=userId;
	}
	public String getUserId(){
		return userId;
	}
	public void setUsername(String username){
		this.username=username;
	}
	public String getUsername(){
		return username;
	}
	public void setMsgSource(byte msgSource){
		this.msgSource=msgSource;
	}
	public byte getMsgSource(){
		return msgSource;
	}
	public void setMsgType(int msgType){
		this.msgType=msgType;
	}
	public int getMsgType(){
		return msgType;
	}
	public void setMsgId(String msgId){
		this.msgId=msgId;
	}
	public String getMsgId(){
		return msgId;
	}
	public void setCreateTime(Timestamp createTime){
		this.createTime=createTime;
	}
	public Timestamp getCreateTime(){
		return createTime;
	}
	public void setLastmodify(Timestamp lastmodify){
		this.lastmodify=lastmodify;
	}
	public Timestamp getLastmodify(){
		return lastmodify;
	}
}

