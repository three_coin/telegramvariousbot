package com.bot.telegramvariousbot.model;

import java.sql.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
/**
 * table name:  tg_draw_activity
 * author name: zhou
 */ 
public class TgDrawActivity{

  public static final RowMapper<TgDrawActivity> ROWMAPPER = BeanPropertyRowMapper.newInstance(TgDrawActivity.class);

  //insert tg_draw_activity (id,activity_code,activity_desc,start_time,end_time,num,draw_admin_user,not_join_username,note,create_time,lastmodify,height,block_hash,lottery_time) values (:id,:activityCode,:activityDesc,:startTime,:endTime,:num,:drawAdminUser,:notJoinUsername,:note,:createTime,:lastmodify,:height,:blockHash,:lotteryTime)

	private int id;
	private String activityCode;
	private String activityDesc;
	private Timestamp startTime;
	private Timestamp endTime;
	private int num;
	private String drawAdminUser;
	private String notJoinUsername;
	private String note;
	private Timestamp createTime;
	private Timestamp lastmodify;
	private String height;
	private String blockHash;
	private Timestamp lotteryTime;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setActivityCode(String activityCode){
		this.activityCode=activityCode;
	}
	public String getActivityCode(){
		return activityCode;
	}
	public void setActivityDesc(String activityDesc){
		this.activityDesc=activityDesc;
	}
	public String getActivityDesc(){
		return activityDesc;
	}
	public void setStartTime(Timestamp startTime){
		this.startTime=startTime;
	}
	public Timestamp getStartTime(){
		return startTime;
	}
	public void setEndTime(Timestamp endTime){
		this.endTime=endTime;
	}
	public Timestamp getEndTime(){
		return endTime;
	}
	public void setNum(int num){
		this.num=num;
	}
	public int getNum(){
		return num;
	}
	public void setDrawAdminUser(String drawAdminUser){
		this.drawAdminUser=drawAdminUser;
	}
	public String getDrawAdminUser(){
		return drawAdminUser;
	}
	public void setNotJoinUsername(String notJoinUsername){
		this.notJoinUsername=notJoinUsername;
	}
	public String getNotJoinUsername(){
		return notJoinUsername;
	}
	public void setNote(String note){
		this.note=note;
	}
	public String getNote(){
		return note;
	}
	public void setCreateTime(Timestamp createTime){
		this.createTime=createTime;
	}
	public Timestamp getCreateTime(){
		return createTime;
	}
	public void setLastmodify(Timestamp lastmodify){
		this.lastmodify=lastmodify;
	}
	public Timestamp getLastmodify(){
		return lastmodify;
	}
	public void setHeight(String height){
		this.height=height;
	}
	public String getHeight(){
		return height;
	}
	public void setBlockHash(String blockHash){
		this.blockHash=blockHash;
	}
	public String getBlockHash(){
		return blockHash;
	}
	public void setLotteryTime(Timestamp lotteryTime){
		this.lotteryTime=lotteryTime;
	}
	public Timestamp getLotteryTime(){
		return lotteryTime;
	}
}

