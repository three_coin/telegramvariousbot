package com.bot.telegramvariousbot.model;

import java.sql.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
/**
 * table name:  tg_draw
 * author name: zhou
 */ 
public class TgDraw{

  public static final RowMapper<TgDraw> ROWMAPPER = BeanPropertyRowMapper.newInstance(TgDraw.class);

  //insert tg_draw (id,draw_day,block,block_height,block_code,open_time,draw_code,user_name,draw_tg_uid,note,create_time,lastmodify,activity_id) values (:id,:drawDay,:block,:blockHeight,:blockCode,:openTime,:drawCode,:userName,:drawTgUid,:note,:createTime,:lastmodify,:activityId)

	private int id;
	private String drawDay;
	private String block;
	private String blockHeight;
	private long blockCode;
	private long openTime;
	private long drawCode;
	private String userName;
	private String drawTgUid;
	private String note;
	private Timestamp createTime;
	private Timestamp lastmodify;
	private int activityId;

	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setDrawDay(String drawDay){
		this.drawDay=drawDay;
	}
	public String getDrawDay(){
		return drawDay;
	}
	public void setBlock(String block){
		this.block=block;
	}
	public String getBlock(){
		return block;
	}
	public void setBlockHeight(String blockHeight){
		this.blockHeight=blockHeight;
	}
	public String getBlockHeight(){
		return blockHeight;
	}
	public void setBlockCode(long blockCode){
		this.blockCode=blockCode;
	}
	public long getBlockCode(){
		return blockCode;
	}
	public void setOpenTime(long openTime){
		this.openTime=openTime;
	}
	public long getOpenTime(){
		return openTime;
	}
	public void setDrawCode(long drawCode){
		this.drawCode=drawCode;
	}
	public long getDrawCode(){
		return drawCode;
	}
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return userName;
	}
	public void setDrawTgUid(String drawTgUid){
		this.drawTgUid=drawTgUid;
	}
	public String getDrawTgUid(){
		return drawTgUid;
	}
	public void setNote(String note){
		this.note=note;
	}
	public String getNote(){
		return note;
	}
	public void setCreateTime(Timestamp createTime){
		this.createTime=createTime;
	}
	public Timestamp getCreateTime(){
		return createTime;
	}
	public void setLastmodify(Timestamp lastmodify){
		this.lastmodify=lastmodify;
	}
	public Timestamp getLastmodify(){
		return lastmodify;
	}
	public void setActivityId(int activityId){
		this.activityId=activityId;
	}
	public int getActivityId(){
		return activityId;
	}
}

