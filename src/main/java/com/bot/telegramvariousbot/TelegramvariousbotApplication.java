package com.bot.telegramvariousbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegramvariousbotApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramvariousbotApplication.class, args);
    }

}
