package com.bot.telegramvariousbot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author zhou
 * @create 2021-12-03 3:50
 * @description:
 */
@Configuration
public class DatabaseConfig {
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.write")
    public DataSource dataSourceW() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.read")
    public DataSource dataSourceR() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public DataSourceTransactionManager txManager(DataSource dataSourceW) {
        return new DataSourceTransactionManager(dataSourceW);
    }

    @Bean
    public JdbcTemplate jdbcTemplateW(DataSource dataSourceW) {
        return new JdbcTemplate(dataSourceW);
    }

    @Bean
    public JdbcTemplate jdbcTemplateR(DataSource dataSourceR) {
        return new JdbcTemplate(dataSourceR);
    }

}