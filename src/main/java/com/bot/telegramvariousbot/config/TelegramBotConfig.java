package com.bot.telegramvariousbot.config;
import com.bot.telegramvariousbot.dao.LuckDrawDaoExt;
import com.bot.telegramvariousbot.service.telegram.bot.LuckDrawBot;
import com.bot.telegramvariousbot.service.telegram.bot.TestBot;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
public class TelegramBotConfig {

    private static Logger logger = LoggerFactory.getLogger(TelegramBotConfig.class);

    @Value("${proxy.http.host}")
    private String proxyHost;
    @Value("${proxy.http.port}")
    private Integer proxyPort;
    @Value("${spring.profiles.active}")
    private String active;

    @Bean
    public TelegramBotsApi initTelegramBot(LuckDrawDaoExt luckDrawDaoExt) {
        logger.info("initTelegramBot:"+active);
        DefaultBotOptions botOptions = new DefaultBotOptions();
        try {
            if (StringUtils.isNotBlank(proxyHost) && StringUtils.isNotBlank(proxyPort.toString())) {
                botOptions.setProxyHost(proxyHost);
                botOptions.setProxyPort(proxyPort);
                //Select proxy type: [HTTP|SOCKS4|SOCKS5] (default: NO_PROXY)
                botOptions.setProxyType(DefaultBotOptions.ProxyType.HTTP);
            }
            CloseableHttpClient httpClient = null;
            WebClient webClient = null;
            if (StringUtils.isNotBlank(proxyHost)) {
                httpClient = HttpClients.custom()
                        .setMaxConnPerRoute(50000)
                        .setMaxConnTotal(100000)
                        .setProxy(new HttpHost(proxyHost, proxyPort))
                        .setUserAgent("th-server/1.0")
                        .build();
                webClient = new WebClient(BrowserVersion.CHROME,proxyHost,proxyPort);
                webClient.getOptions().setThrowExceptionOnScriptError(false);//当JS执行出错的时候是否抛出异常, 这里选择不需要
                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常, 这里选择不需要
                webClient.getOptions().setActiveXNative(false);
                webClient.getOptions().setCssEnabled(false);//是否启用CSS, 因为不需要展现页面, 所以不需要启用
                webClient.getOptions().setJavaScriptEnabled(false); //很重要，启用JS
                webClient.setAjaxController(new NicelyResynchronizingAjaxController());//很重要，设置支持AJAX

            } else {
                httpClient = HttpClients.custom()
                        .setMaxConnPerRoute(50000)
                        .setMaxConnTotal(100000)
                        .setUserAgent("th-server/1.0")
                        .build();
                webClient = new WebClient(BrowserVersion.CHROME);
                webClient.getOptions().setThrowExceptionOnScriptError(false);//当JS执行出错的时候是否抛出异常, 这里选择不需要
                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常, 这里选择不需要
                webClient.getOptions().setActiveXNative(false);
                webClient.getOptions().setCssEnabled(false);//是否启用CSS, 因为不需要展现页面, 所以不需要启用
                webClient.getOptions().setJavaScriptEnabled(false); //很重要，启用JS
                webClient.setAjaxController(new NicelyResynchronizingAjaxController());//很重要，设置支持AJAX

            }
            DefaultBotSession defaultBotSession = new DefaultBotSession();
            defaultBotSession.setOptions(botOptions);
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(defaultBotSession.getClass());
            if ("prod".equals(active)) {
                //telegramBotsApi.registerBot(new LuckDrawBot(webClient,httpClient,luckDrawDaoExt, botOptions));
                //telegramBotsApi.registerBot(new TestBot(botOptions));
            }
            if ("loc".equals(active)) {
                telegramBotsApi.registerBot(new LuckDrawBot(webClient,httpClient,luckDrawDaoExt, botOptions));
                //telegramBotsApi.registerBot(new TestBot(botOptions));
            }
            if ("test".equals(active)) {
                //telegramBotsApi.registerBot(new LuckDrawBot(webClient,httpClient,luckDrawDaoExt, botOptions));
                //telegramBotsApi.registerBot(new TestBot(botOptions));
            }
            return telegramBotsApi;
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        return null;
    }

}