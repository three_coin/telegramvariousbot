package com.bot.telegramvariousbot.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhou
 * @create 2021-12-03 3:51
 * @description:
 */
@Configuration
public class HttpConfig {

    @Value("${proxy.http.host}")
    private String proxyHost;
    @Value("${proxy.http.port}")
    private Integer proxyPort;
    @Value("${spring.profiles.active}")
    private String active;

    @Bean(initMethod = "start", destroyMethod = "close")
    public CloseableHttpAsyncClient httpAsyncClient() {
        return HttpAsyncClients.custom()
                .setMaxConnPerRoute(50000)
                .setMaxConnTotal(100000)
                .setUserAgent("th-server/1.0")
                .build();
    }

    @Bean(destroyMethod = "close")
    public CloseableHttpClient httpClient() {
        if (StringUtils.isNotBlank(proxyHost)) {
            return HttpClients.custom()
                    .setMaxConnPerRoute(50000)
                    .setMaxConnTotal(100000)
                    .setProxy(new HttpHost(proxyHost, proxyPort))
                    .setUserAgent("th-server/1.0")
                    .build();
        } else {
            return HttpClients.custom()
                    .setMaxConnPerRoute(50000)
                    .setMaxConnTotal(100000)
                    .setUserAgent("th-server/1.0")
                    .build();
        }
    }

}