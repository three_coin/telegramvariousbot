package com.bot.telegramvariousbot.service.telegram.bot;



import com.bot.telegramvariousbot.service.telegram.config.TestBotConfig;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class TestBot extends TelegramLongPollingBot {

    public TestBot() {
        this((DefaultBotOptions) new DefaultBotOptions());
    }

    public TestBot(DefaultBotOptions options) {
        super(options);
    }

    public void onUpdateReceived(Update update) {
        SendMessage message = new SendMessage();
        message.setChatId(update.getMessage().getChatId().toString());
        message.setText("pong!");
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getBotUsername() {
        return  TestBotConfig.BOOT_NAME_TEXT;
    }


    public String getBotToken() {
        return TestBotConfig.TOKEN_TEXT;
    }
}
